# DogDental

Dog dental WebGl application

## Pre-requisite
1) Install [git-lfs](https://git-lfs.github.com/)
2) Install WebGl module to build the app for web browser. [Unity-WebGl-Setup](http://download.unity3d.com/download_unity/5c3fb0a11183/TargetSupportInstaller/UnitySetup-WebGL-Support-for-Editor-2019.3.6f1.exe) 


## Project Description: 
Dental Dog is an interactive 3D model of a canine skull and teeth, used to teach veterinary dentistry to current veterinary students and as a supplemental teaching tool in a MOOK. 
The application provides various features that allow the user to understand the internal structures of a canine skull and teeth. The user can interact with the teeth and learn the names of each tooth. 
They can learn the differences between each tooth in terms of number of roots and the direction they arch toward. They can rotate the skull to observe from different angles. 
The skull can be switched between an opaque and transparent mode (where the tooth roots and tooth coloring can be viewed), and the jaw can be opened and closed, to observe the underlying structures.

## Additional information
The project is built as a WebGL application. To run the project locally, open index.html in a browser (note: Chrome can not run the project locally. This is explained at https://docs.unity3d.com/Manual/webgl-building.html). 
You can view our hosted version of the project at https://nitishtalasu.github.io/DogDentalBuild  (this will work properly in Chrome).

Our video explaining the features of the project can be found at https://www.youtube.com/watch?v=1KXj-mxzFtU&feature=youtu.be


