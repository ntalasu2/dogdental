﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

// This script toggles the images on the menu buttons based on user interaction

public class ImageChange : MonoBehaviour
{
    public static void ChangeButtonImageAndText(string tag, Sprite img, string text)
    {
        GameObject.FindGameObjectWithTag(tag).GetComponent<Button>().GetComponent<Image>().sprite = img;
        GameObject.FindGameObjectWithTag(tag).GetComponent<Button>().GetComponentInChildren<TextMeshProUGUI>().text = text;
    }
}
