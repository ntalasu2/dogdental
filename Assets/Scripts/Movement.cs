﻿using UnityEngine;

// This script allows the skull to be dragged in different directions by using the mouse left click

public class Movement : MonoBehaviour
{
    public Transform target;
    public Transform startObject;
    public float dragSpeed = 2;
    private Vector3 dragOrigin;
    private Vector3 cameraStart;
    public bool cameraDragging = true;

    public float maxLeft = -10f;
    public float maxRight = 10f;
    public float maxUp = 5f;
    public float maxDown = -5f;

    // Start is called before the first frame update
    void Start()
    {
        target.transform.position = startObject.transform.position;
        cameraStart = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        float left = Screen.width * 0.2f;
        float right = Screen.width - (Screen.width * 0.2f);

        if (mousePosition.x < left)
        {
            cameraDragging = true;
        }
        else if (mousePosition.x > right)
        {
            cameraDragging = true;
        }

        if (cameraDragging)
        {

            if (Input.GetMouseButtonDown(0))
            {
                dragOrigin = Input.mousePosition;
                return;
            }

            if (!Input.GetMouseButton(0)) return;

            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
            Vector3 move = new Vector3(-pos.x * dragSpeed * Time.deltaTime, -pos.y * dragSpeed * Time.deltaTime, 0);
            target.transform.rotation = transform.rotation;

            if (move.x > 0f && move.y > 0f)
            {
                if (target.transform.position.x < maxRight && target.transform.position.y < maxUp)
                {
                    target.transform.Translate(move);
                    transform.Translate(move);
                }
            }
            else
            {
                if (target.transform.position.x > maxLeft && target.transform.position.y > maxDown)
                {
                    target.transform.Translate(move);
                    transform.Translate(move);
                }
            }

        }
    }
}
