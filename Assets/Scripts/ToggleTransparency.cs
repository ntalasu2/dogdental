﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// This script toggles the model between opaque and transparent mode based on user interaction

public class ToggleTransparency : MonoBehaviour
{
    public GameObject opaque;
    public GameObject transparent;
    public GameObject outside;
    public GameObject inside;
    public GameObject mandible;
    public Material opaqueMaterial;
    public Material transparentMaterial;
    public static bool isOpaque;
    public Sprite showRoots;
    public Sprite hideRoots;

    // Start is called before the first frame update
    void Start()
    {
        isOpaque = true;
        opaque.gameObject.SetActive(true);
        transparent.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
           Toggle();
        }
    }
    public void Toggle()
    {

        //opaque.gameObject.SetActive(!isOpaque);
        //transparent.gameObject.SetActive(isOpaque);
        isOpaque = !isOpaque;
        if (isOpaque)
        {
            inside.GetComponent<Renderer>().material = opaqueMaterial;
            outside.GetComponent<Renderer>().material = opaqueMaterial;
            mandible.GetComponent<Renderer>().material = opaqueMaterial;
            ImageChange.ChangeButtonImageAndText("ShowRoots", hideRoots, "Show Roots");
        }
        else
        {
            inside.GetComponent<Renderer>().material = transparentMaterial;
            outside.GetComponent<Renderer>().material = transparentMaterial;
            mandible.GetComponent<Renderer>().material = transparentMaterial;
            ImageChange.ChangeButtonImageAndText("ShowRoots", showRoots, "Hide Roots");
        }
    }
}
