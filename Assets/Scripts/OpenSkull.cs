﻿using UnityEngine;

// This script performs the open/close of the jaw by rotating the upper skull by a certain angle

public class OpenSkull : MonoBehaviour
{

    private Quaternion initialRotation;
    public float angle = 30;
    private bool isJawOpen = false;
    public GameObject[] upperSkullObjs;
    public Transform opaqueSkullObj;
    public Sprite openImage;
    public Sprite closeImage;

    // Start is called before the first frame update
    void Start()
    {
        initialRotation = opaqueSkullObj.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            OpenJaw();
        }

        SetSkulls();
    }

    public void OpenJaw()
    {
        isJawOpen = !isJawOpen;
    }

    public void SetSkulls()
    {
        if (!isJawOpen)
        {
            SetSkullsRotations(initialRotation);
            ImageChange.ChangeButtonImageAndText("OpenJaw", openImage, "Open Jaw");
        }
        else
        {
            SetSkullsRotations(Quaternion.Euler(0, 0, angle));
            ImageChange.ChangeButtonImageAndText("OpenJaw", closeImage, "Close Jaw");
        }
    }

    public void SetSkullsRotations(Quaternion rotation)
    {
        upperSkullObjs = GameObject.FindGameObjectsWithTag("UpperSkull");
        // Debug.Log("Size is" + upperSkullObjs.Length);
        foreach (GameObject obj in upperSkullObjs)
        {
            obj.transform.rotation = rotation;
        }
    }
}
