﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

// This script colors all the teeth (red, blue, green, and striped) by updating the material attached to the teeth

public class ColorTeeth : MonoBehaviour
{
    public GameObject[] greenTeeth;
    public GameObject[] redTeeth;
    public GameObject[] blueTeeth;
    public GameObject toothSampleInitial;
    private Material originalMaterial;
    private Material green;
    private Material blue;
    private Material red;
    public Material WhiteUnchosenTooth;
    public Material ToothStripes;
    private bool isGreen;
    private bool isBlue;
    private bool isRed;
    public static bool isActive;
    public static bool areColored;
    public Sprite showColor;
    public Sprite hideColor;
    public Button showButton;
    public Button stripesButton;

    // Start is called before the first frame update
    void Start()
    {
        areColored = false;
        isGreen = false;
        isBlue = false;
        isRed = false;
        isActive = false;
        showButton.onClick.AddListener(colorAll);
        stripesButton.onClick.AddListener(Stripe);

    }

    void colorGreen()
    {
        if(isGreen)
        {
            for(int i = 0; i < greenTeeth.Length; i++) 
            {
                greenTeeth[i].GetComponentInChildren<Renderer>().sharedMaterial = WhiteUnchosenTooth;
            }
        }
        else
        {
            for(int i = 0; i < greenTeeth.Length; i++)
            {
                
                green = (Material)Resources.Load("Materials/ChooseGreen", typeof(Material));
                greenTeeth[i].GetComponentInChildren<Renderer>().sharedMaterial = green;
            }
        }
    }

    void colorBlue()
    {
        if(isBlue)
        {
            for(int i = 0; i < blueTeeth.Length; i++) 
            {
                blueTeeth[i].GetComponentInChildren<Renderer>().sharedMaterial = WhiteUnchosenTooth;
            }
        }
        else
        {
            for(int i = 0; i < blueTeeth.Length; i++)
            {
                blue = (Material)Resources.Load("Materials/ChooseBlue", typeof(Material));
                blueTeeth[i].GetComponentInChildren<Renderer>().sharedMaterial = blue;
            }
        }
    }

    void colorRed()
    {
        if(isRed)
        {
            for(int i = 0; i < redTeeth.Length; i++) 
            {
                redTeeth[i].GetComponentInChildren<Renderer>().sharedMaterial = WhiteUnchosenTooth;
            }
        }
        else
        {
            for(int i = 0; i < redTeeth.Length; i++)
            {
                red = (Material)Resources.Load("Materials/ChooseRed", typeof(Material));
                redTeeth[i].GetComponentInChildren<Renderer>().sharedMaterial = red;
            }
        }
    }
    public void colorAll()
    {
        if (areColored)
        {
            ImageChange.ChangeButtonImageAndText("ShowColors", showColor, "Show Root Number Colors");
        }
        else
        {
            ImageChange.ChangeButtonImageAndText("ShowColors", hideColor, "Hide Root Number Colors");
        }

        areColored = !areColored;
        colorGreen();
        isGreen = !isGreen;
        colorBlue();
        isBlue = !isBlue;
        colorRed();
        isRed = !isRed;

        if (isActive && !areColored)
        {
            isActive = !isActive;
            Stripe();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            colorGreen();
            isGreen = !isGreen;
            colorBlue();
            isBlue = !isBlue;
            colorRed();
            isRed = !isRed;
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            colorGreen();
            isGreen = !isGreen;
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            colorBlue();
            isBlue = !isBlue;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            colorRed();
            isRed = !isRed;
        }
    }
    public void Stripe()
    {
        isActive = !isActive;
        if (isActive && !areColored)
        {
            for (int i = 0; i < redTeeth.Length; i++)
            {
                redTeeth[i].GetComponent<MeshRenderer>().material = ToothStripes;
            }
            for (int i = 0; i < greenTeeth.Length; i++)
            {
                greenTeeth[i].GetComponent<MeshRenderer>().material = ToothStripes;
            }
            for (int i = 0; i < blueTeeth.Length; i++)
            {
                blueTeeth[i].GetComponent<MeshRenderer>().material = ToothStripes;
            }
        }
        else if (!areColored)
        {
            for (int i = 0; i < redTeeth.Length; i++)
            {
                redTeeth[i].GetComponent<MeshRenderer>().material = WhiteUnchosenTooth;
            }
            for (int i = 0; i < greenTeeth.Length; i++)
            {
                greenTeeth[i].GetComponent<MeshRenderer>().material = WhiteUnchosenTooth;
            }
            for (int i = 0; i < blueTeeth.Length; i++)
            {
                blueTeeth[i].GetComponent<MeshRenderer>().material = WhiteUnchosenTooth;
            }
        }

    }
}
