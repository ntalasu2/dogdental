﻿using System.Collections;
using System.Collections.Generic;

// This script works in conjunction with SelectTooth.cs. This script provides a dictionary of all
// the tooth labels. When a tooth is highlighted, the particular tooth object gets its corresponding
// label through this dictionary and displays it on the app.

public static class TeethNames
{
    // Teeth name table
    public static IDictionary<string, string> name_table = new Dictionary<string, string>()
        {
            {"IncisorULCentral_I1 1","Right Maxillary 1st Incisor (101"},
            {"IncisorUL2_I2 1","Right Maxillary 2nd Incisor (102)"},
            {"IncisorUL3_I3 1","Right Maxillary 3rd Incisor (103)"},
            {"CanineUL_C1 1", "Right Maxillary Canine (104)"},
            {"PreMolarUL1_P1", "Right Maxillary 1st Premolar (105)"},
            {"PreMolarUL2_P3", "Right Maxillary 2nd Premolar (106)"},
            {"UL_PM5", "Right Maxillary 3rd Premolar (107)" },
            {"WholeUP4R1", "Right Maxillary 4th Premolar (108)" },
            {"MolarUL_M24", "Right Maxillary 1st Molar (109)"},
            {"MolarUL_M2 1", "Right Maxillary 2nd Molar (110)"},
            {"IncisorULCentral_I1","Left Maxillary 1st Incisor (201)"},
            {"IncisorUL2_I2","Left Maxillary 2nd Incisor (202)"},
            {"IncisorUL3_I3","Left Maxillary 3rd Incisor (203)"},
            {"CanineUL_C1", "Left Maxillary Canine (204)"},
            {"PreMolarUL1_P2", "Left Maxillary 1st Premolar (205)"},
            {"PreMolarUL2_P2", "Left Maxillary 2nd Premolar (206)"},
            {"UL_PM6", "Left Maxillary 3rd Premolar (207)" },
            {"MolarUL_P4whole3", "Left Maxillary 4th Premolar (208)" },
            {"MolarUL_M25", "Left Maxillary 1st Molar (209)"},
            {"MolarUL_M2", "Left Maxillary 2nd Molar (210)"},
            {"IncisorLLCentral_I1","Right Mandibular 1st Incisor (401)"},
            {"IncisorLL2_I2","Right Mandibular 2nd Incisor (402)"},
            {"IncisorLL3_I3","Right Mandibular 3rd Incisor (403)"},
            {"CanineLL_C1", "Right Mandibular Canine (404)"},
            {"PreMolarLL1_P1", "Right Mandibular 1st Premolar (405)"},
            {"PreMolarLL2_P2", "Right Mandibular 2nd Premolar (406)"},
            {"PreMolarLL3_P4", "Right Mandibular 3rd Premolar (407)" },
            {"PreMolarLL4_P4", "Right Mandibular 4th Premolar (408)" },
            {"polySurface109", "Right Mandibular 1st Molar (409)"},
            {"MolarLL_M2", "Right Mandibular 2nd Molar (410)"},
            {"MolarLL_M3", "Right Mandibular 3rd Molar (411)" },
            {"IncisorLLCentral_I1 1","Left Mandibular 1st Incisor (301)"},
            {"IncisorLL2_I2 1","Left Mandibular 2nd Incisor (302)"},
            {"IncisorLL3_I3 1","Left Mandibular 3rd Incisor (303)"},
            {"CanineLL_C1 1", "Left Mandibular Canine (304)"},
            {"PreMolarLL1_P1 1", "Left Mandibular 1st Premolar (305)"},
            {"PreMolarLL2_P2 1", "Left Mandibular 2nd Premolar (306)"},
            {"PreMolarLL3_P4 1", "Left Mandibular 3rd Premolar (307)" },
            {"PreMolarLL4_P4 1", "Left Mandibular 4th Premolar (308)" },
            {"polySurface109 1", "Left Mandibular 1st Molar (309)"},
            {"MolarLL_M2 1", "Left Mandibular 2nd Molar (310)"},
            {"MolarLL_M3 1", "Left Mandibular 3rd Molar (311)" }
        };
}
