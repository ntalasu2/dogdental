﻿using UnityEngine;

// This script activates/deactivates the directional markers by activating/deactivating the corresponding game object

public class ActivateDirectionMarkers : MonoBehaviour
{
    public GameObject directionMarkerOpaque;
    public GameObject directionMarkerTransparent;

    public Sprite showDirectionMarkers;
    public Sprite hideDirectionMarkers;

    private bool isActive;
    // Start is called before the first frame update
    void Start()
    {
        isActive = false;
        directionMarkerOpaque.gameObject.SetActive(false);
        directionMarkerTransparent.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.D))
        {
            showDirections();
        }
    }
    public void showDirections()
    {
        directionMarkerOpaque.gameObject.SetActive(!isActive);
        directionMarkerTransparent.gameObject.SetActive(!isActive);      
        isActive = !isActive;
        if (isActive)
        {
            ImageChange.ChangeButtonImageAndText("ShowDirections", hideDirectionMarkers, "Hide Directions");
        }
        else
        {
            ImageChange.ChangeButtonImageAndText("ShowDirections", showDirectionMarkers, "Show Directions");
        }
    }
}
