﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script, when activated, shows all the tooth numbers on the model

public class ShowNumbers : MonoBehaviour
{
    public GameObject g100;
    public GameObject g200;
    public GameObject g300;
    public GameObject g400;
    private bool isActive;
    // Start is called before the first frame update
    void Start()
    {
        isActive = false;
        g100.gameObject.SetActive(false);
        g200.gameObject.SetActive(false);
        g300.gameObject.SetActive(false);
        g400.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void showNumber()
    {
        g100.gameObject.SetActive(!isActive);
        g200.gameObject.SetActive(!isActive);
        g300.gameObject.SetActive(!isActive);
        g400.gameObject.SetActive(!isActive);
        isActive = !isActive;
    }
}
