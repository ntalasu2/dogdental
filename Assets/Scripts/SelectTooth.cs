﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Versioning;
using UnityEngine;
using UnityEngine.UI;

// This script highlights a particular tooth on hovering the mouse over it, and also shows its
// tooth root color(red, blue or green). It goes back to its original material on mouse exit

public class SelectTooth : MonoBehaviour
{

    private Material start_material;
    private Material striped_material;
    private Material red_material;
    private Material green_material;
    private Material blue_material;
    private Material red_highlight;
    private Material blue_highlight;
    private Material green_highlight;
    private Material outline_material;
    private Material start_color_material;
    private Renderer rend;
    private Text tooth_name;
    private bool is_opaque;
    public bool is_red;
    public bool is_blue;
    public bool is_green;
    private bool is_label_on;

    // Start is called before the first frame update
    void Start()
    {

        rend = GetComponent<Renderer>();
        start_material = new Material(rend.material);
        striped_material = Resources.Load("Materials/ToothStripes", typeof(Material)) as Material;
        outline_material = Resources.Load("Materials/OutlineMaterial", typeof(Material)) as Material;
        red_material = Resources.Load("Materials/ChooseRed", typeof(Material)) as Material;
        red_highlight = Resources.Load("Materials/RedOutline", typeof(Material)) as Material;
        blue_material = Resources.Load("Materials/ChooseBlue", typeof(Material)) as Material;
        blue_highlight = Resources.Load("Materials/BlueOutline", typeof(Material)) as Material;
        green_material = Resources.Load("Materials/ChooseGreen", typeof(Material)) as Material;
        green_highlight = Resources.Load("Materials/GreenOutline", typeof(Material)) as Material;
        tooth_name = GameObject.Find("NameCanvas/ToothName").GetComponent<Text>();
        tooth_name.text = "";
        is_label_on = false;
    }

    void OnMouseOver()
    {
        if (Menu.show_teeth_labels)
        {
            is_label_on = true;        
            if (is_red)
            {
                rend.material = red_highlight;
                start_color_material = red_material;

            }
            else if (is_blue)
            {
                rend.material = blue_highlight;
                start_color_material = blue_material;
            }
            else if (is_green)
            {
                rend.material = green_highlight;
                start_color_material = green_material;
            }
        
            if (TeethNames.name_table.ContainsKey(gameObject.name)) // Handle top teeth
            {
                tooth_name.text = TeethNames.name_table[gameObject.name];
            }
            else // Handle bottom teeth
            {
                tooth_name.text = TeethNames.name_table[transform.parent.name];
            }
        }
    }

    void OnMouseExit()
    {
        if (is_label_on)
        {
            if (ColorTeeth.areColored)
            {
                rend.material = start_color_material;
            }
            else if (ColorTeeth.isActive)
            {
                rend.material = striped_material;
            }
            else
            {
                rend.material = start_material;
            }
            tooth_name.text = "";
            is_label_on = false;
        }
    }
}
