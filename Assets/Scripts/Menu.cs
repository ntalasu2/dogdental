﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script allows the menu panel to be displayed using an Animator

public class Menu : MonoBehaviour
{
    public GameObject MenuPanel;
    public static bool show_teeth_labels;
    public Sprite showLabels;
    public Sprite hidelabels;

    void start()
    {
        show_teeth_labels = false;
    }

    public void ShowMenuPanel()
    {
        Animator animator = MenuPanel.GetComponent<Animator>();
        bool isOpen = animator.GetBool("Show");
        animator.SetBool("Show", !isOpen);
    }

    public void ShowToothLabels()
    {
        if (show_teeth_labels)
        {
            ImageChange.ChangeButtonImageAndText("ShowLabels", showLabels, "Show Labels");
        }
        else
        {
            ImageChange.ChangeButtonImageAndText("ShowLabels", hidelabels, "Hide Labels");
        }
        show_teeth_labels = !show_teeth_labels;
    }
}
